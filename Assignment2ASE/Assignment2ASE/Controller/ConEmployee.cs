﻿using Assignment2ASE.Controller.Database;
using Assignment2ASE.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.Controller
{
    class ConEmployee
    {
        DBConnection dbc = new DBConnection();
        MySqlConnection conn;
        public bool Insertdata(EmployeeModel em, byte[] arr)
        {
            try
            {
                string sqlquery = "insert into employee(emp_code,emp_name,contact_no,emp_email,address,emo_designation,emp_position,emp_image) values('" +em.GetEmployeeCode() + "','" + em.GetName() + "','" + em.GetContactno() + "','" + em.GetEmail() + "','" + em.GetAddress() + "','" + em.GetDesignation() + "','" + em.GetPosition() + "','" +arr+ "')";
                string sqlquery1 = "insert into user(username,password,usertype) values('"+em.GetUsername()+ "','" + em.GetPassword() + "','" + em.GetUsertype() + "');";
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                MySqlCommand mycommand1 = new MySqlCommand(sqlquery1, conn);

                int num = mycommand.ExecuteNonQuery();
                int num1 = mycommand1.ExecuteNonQuery();
                if (num > 0 && num1>1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable AllEmployee()
        {
            DataTable table = new DataTable();
            conn = dbc.GetConnection();
            string query = "select * from employee";
            try
            {
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = mycommand;
                adapter.Fill(table);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " +ex.Message);
            }
            return table;
        }
        public bool Deletedata(EmployeeModel em)
        {
            try
            {
                string sqlquery = "delete from employee where emp_code='"+em.GetEmployeeCode()+"'";
                
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                

                int num = mycommand.ExecuteNonQuery();
                
                if (num > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }
        }
        public void Search(string search,EmployeeModel ee)
        {
            try
            {
                string sqlquery = "select * from employee where emp_code='" + search + "'";
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                MySqlDataReader reader = mycommand.ExecuteReader();




                if (reader.Read())
                {
                    ee.SetEmployeeCode(reader.GetString("emp_code"));
                    ee.SetName(reader.GetString("emp_name"));
                    ee.SetContactno(reader.GetString("contact_no"));
                    ee.SetEmail(reader.GetString("emp_email"));
                    ee.SetAddress(reader.GetString("address"));
                    ee.SetDesigntion(reader.GetString("emo_designation"));

                    //m.setJoinedDate(reader.GetString("joined_date"));

                    ee.SetPosition(reader.GetString("emp_position"));



                }
            }
            finally
            {
                conn.Close();
            }


        }
        public bool UpdateEmployee(EmployeeModel em)
        {
            try
            {
                string sqlquery = "update employee set emp_name='"+em.GetName()+"',contact_no ='"+em.GetContactno()+"',emp_email='"+em.GetEmail()+"',address='"+em.GetAddress()+"',emo_designation='"+em.GetDesignation()+"',emp_position='"+em.GetPosition()+"'";
               
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
               

                int num = mycommand.ExecuteNonQuery();
               
                if (num > 0 )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
