﻿using Assignment2ASE.Controller.Database;
using Assignment2ASE.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.Controller
{
    class ConManager
    {
        DBConnection dbc = new DBConnection();
        MySqlConnection conn;
        public bool Insertdata(string code,string name, string address, string contactno, string email, string position, string department)
        {
            try
            {
                string sqlquery = "insert into manager(mgr_code,name,department,address,contact_no, email, position) values('" + code + "','" + name + "','" + address + "','" + contactno + "','" + email + "','" + position + "','" + department + "')";

                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                int num = mycommand.ExecuteNonQuery();
                if (num > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }

        }
        public bool InsertintoUser(string username, string password, string usertype) {
            try
            {
                string sqlquery = "insert into user(username,password,usertype) values('" + username + "','" + password + "','" + usertype + "')";

                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                int num = mycommand.ExecuteNonQuery();
                if (num > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }
        }

        public void Search(ManagerModel m) {
            try
            {
                string sqlquery = "select * from manager where mgr_code='" + m.getSearchItem() + "'";
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                MySqlDataReader reader = mycommand.ExecuteReader();




                if (reader.Read())
                {
                    m.setManagerId(reader.GetInt32("manager_id"));
                    m.setManagerCode(reader.GetString("mgr_code"));
                    m.setManagerName(reader.GetString("name"));
                    m.setDepartment(reader.GetString("department"));
                    m.setAddrress(reader.GetString("address"));
                    m.setContactNo(reader.GetString("email"));
                    //m.setJoinedDate(reader.GetString("joined_date"));
                    
                    m.setPosition(reader.GetString("position"));



                }
            }
            finally
            {
                conn.Close();
            }


        }
        public void Delete(ManagerModel m)
        {
            try
            {
                string sqlquery = "delete from manager where mgr_code='" + m.getManagerCode() + "'";
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                MySqlDataReader reader = mycommand.ExecuteReader();
                MessageBox.Show("Manager Deleted Successfully");
            }
            finally
            {
                conn.Close();
            }


        }
        public void Update(ManagerModel m)
        {
            try
            {
                string sqlquery = "update manager set name='" + m.getManagerName() + "',department='" + m.getDepartment() + "',address='" + m.getAddress() + "',contact_no='" + m.getContactNo() + "',email='" + m.getEmail() + "',position='" + m.getPosition() + "' where mgr_code='"+m.getManagerCode()+"'";
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                MySqlDataReader reader = mycommand.ExecuteReader();
                MessageBox.Show("Manager Details Updated Successfully");



                if (reader.Read())
                {
                   



                }
            }
            finally
            {
                conn.Close();
            }


        }
        public DataTable AllManager()
        {
            DataTable table = new DataTable();
            conn = dbc.GetConnection();
            string query = "select mgr_code, name, department,address,contact_no,email,position from manager";
            try
            {
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = mycommand;
                adapter.Fill(table);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return table;
        }
    }
}
