﻿using Assignment2ASE.Controller.Database;
using Assignment2ASE.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.Controller
{
    class ConProject
    {
        DBConnection dbc = new DBConnection();
        MySqlConnection conn;

        public bool InsertProject(ProjectModel pm)
        {
            try
            {
                string sqlquery = "insert into project(project_id,project_title, manager,developer,tester) values('" + pm.GetProjectCode() + "','" + pm.GetProjectTitle()+ "','" + pm.GetManager() + "','" + pm.GetDeveloper() + "','" + pm.GetTester() + "')";
               
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
              
                int num = mycommand.ExecuteNonQuery();
             
                if (num > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }


        }
        public DataTable AllProject()
        {
            DataTable table = new DataTable();
            conn = dbc.GetConnection();
            string query = "select * from project";
            try
            {
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = mycommand;
                adapter.Fill(table);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return table;
        }
        public void Search(string search,ProjectModel pm )
        {
            try
            {
                string sqlquery = "select * from project where project_id='" + search + "'";
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                MySqlDataReader reader = mycommand.ExecuteReader();




                if (reader.Read())
                {
                    pm.SetProjectCode(reader.GetString("project_id"));
                    pm.SetProjectTitle(reader.GetString("project_title"));
                    pm.SetManager(reader.GetString("manager"));
                    pm.SetDeveloper(reader.GetString("developer"));
                    pm.SetTester(reader.GetString("tester"));
                   



                }
            }
            finally
            {
                conn.Close();
            }


        }
        public void Delete(string projectCode)
        {
            try
            {
                string sqlquery = "delete from project where project_id='" + projectCode + "'";
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                MySqlDataReader reader = mycommand.ExecuteReader();
                MessageBox.Show("Prroject Deleted Successfully");
            }
            finally
            {
                conn.Close();
            }


        }
        public bool UpdateProject(ProjectModel pm)
        {
            try
            {
                string sqlquery = "update project set project_title ='" + pm.GetProjectTitle() + "',manager='" + pm.GetManager() + "',developer='" + pm.GetDeveloper() + "',tester='" + pm.GetTester() + "'where project_id='"+pm.GetProjectCode()+"'";

                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);


                int num = mycommand.ExecuteNonQuery();

                if (num > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
