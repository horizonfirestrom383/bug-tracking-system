﻿using Assignment2ASE.Controller.Database;
using Assignment2ASE.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.Controller
{
    class ConBug
    {
        DBConnection dbc = new DBConnection();
        MySqlConnection conn;
        public bool InsertBug(BugModel bm, byte[] arr)
        {
            try
            {
                string sqlquery = "insert into bug(bug_code, bug_status, bug_title,bugLines, bugMethod,bug_description, project, class, Attachment) values('"+bm.GetBugCode()+ "','" + bm.GetBugStatus() + "','" + bm.GetBugTitle() + "','" + bm.GetBugLines() + "','" + bm.GetBugMethod() + "','" + bm.GetBugDescription() + "','" + bm.GetProject() + "','" + bm.GetBugClass() + "','" +arr+ "')";
               
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                

                int num = mycommand.ExecuteNonQuery();
                
                if (num > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }
        }
        public DataTable AllBugs()
        {
            DataTable table = new DataTable();
            conn = dbc.GetConnection();
            string query = "select * from bug";
            try
            {
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = mycommand;
                adapter.Fill(table);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return table;
        }
        public DataTable FixedBug()
        {
            DataTable table = new DataTable();
            conn = dbc.GetConnection();
            string status = "Fixed";
            string query = "select * from bug where bug_status='"+status+"'";
            try
            {
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = mycommand;
                adapter.Fill(table);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return table;
        }
        public DataTable UnFixedBugs()
        {
            DataTable table = new DataTable();
            string status = "Unfixed";
            conn = dbc.GetConnection();
            string query = "select * from bug where bug_status='"+status+"' ";
            try
            {
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = mycommand;
                adapter.Fill(table);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return table;
        }
        public void Search(string searchItem, BugModel bm)
        {
            try
            {
                string sqlquery = "select * from bug where bug_code='" + searchItem + "'";
                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);
                MySqlDataReader reader = mycommand.ExecuteReader();




                if (reader.Read())
                {
                    bm.SetBugCode(reader.GetString("bug_code"));
                    bm.SetBugStatus(reader.GetString("bug_status"));
                    bm.SetBugTitle(reader.GetString("bug_title"));
                    bm.SetBugLines(reader.GetString("bugLines"));
                    bm.SetBugMethod(reader.GetString("bugMethod"));
                    bm.SetBugDescription(reader.GetString("bug_description"));
                    bm.SetProject(reader.GetString("project"));
                    bm.SetBugClass(reader.GetString("class"));

                }
            }
            finally
            {
                conn.Close();
            }


        }
        public bool Deletedata(string bugCode)
        {
            try
            {
                string sqlquery = "delete from bug where bug_code='" + bugCode + "'";

                conn = dbc.GetConnection();
                conn.Open();
                MySqlCommand mycommand = new MySqlCommand(sqlquery, conn);


                int num = mycommand.ExecuteNonQuery();

                if (num > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }
        }

    }
}
