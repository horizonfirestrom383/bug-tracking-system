﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2ASE.Model
{
    class EmployeeModel
    {
        string code, name,username, password, usertype, contactno, email, address, designation, position;

        public void SetEmployeeCode(string code) {
            this.code = code;


        }
        public string GetEmployeeCode() {
            return code;
        }
        public void SetName(string name) {
            this.name = name;
        }
        public string GetName() {
            return name;
        }
        public void SetUsername(string username)
        {
            this.username = username;
        }
        public string GetUsername()
        {
            return username;
        }
        public void SetPassword(string password) {
            this.password = password;
        }
        public string GetPassword() {
            return password;
        }
        public void SetUsertype(string usertype) {
            this.usertype = usertype;
        }
        public string GetUsertype() {
            return usertype;
        }
        public void SetContactno(string contactno) {
            this.contactno = contactno;
        }
        public string GetContactno() {
            return contactno;
        }
        public void SetEmail(string email) {
            this.email = email;
        }

        public string GetEmail() {
            return email;
        }
        public void SetAddress(string address) {
            this.address = address;
        }
        public string GetAddress() {
            return address;
        }
        public void SetDesigntion(string designation) {
            this.designation = designation;
        }
        public string GetDesignation() {
            return designation;
        }
        public void SetPosition(string position) {
            this.position = position;
        }
        public string GetPosition() {
            return position;
        }


    }
}
