﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.Controller.Database
{
    class DBConnection
    {
        private static MySqlConnection con = null;
        public  MySqlConnection GetConnection() {
            if (con == null) {
                ConnectToDb();
            }
            return con;
        }

        private MySqlConnection ConnectToDb()
        {
            string ConnectString = "datasource = localhost; username = root; password=; database = md; SSLmode = none;";
            con = new MySqlConnection(ConnectString);
           /* try
            {
                con.Open();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            */
            return con;
        }
        
    }
   
}
