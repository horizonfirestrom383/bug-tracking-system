﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2ASE.Model
{
    class ProjectModel
    {
        string project_title, start_date,manager, developer, tester, projectCode;
        public void SetProjectCode(string projectCode)
        {
            this.projectCode = projectCode;
        }
        public string GetProjectCode()
        {
            return projectCode;
        }

        public void SetProjectTitle(string title) {
            this.project_title = title;
        }
        public string GetProjectTitle() {
            return project_title;
        }

        public void SetManager(string manager)
        {
            this.manager = manager;
        }
        public string GetManager()
        {
            return manager;
        }
        public void SetProjectStartDate(string date) {
            this.start_date = date;
        }
        public string GetProjectStartDate() {
            return start_date;
        }
        public void SetDeveloper(string developer) {
            this.developer = developer;
        }
        public string GetDeveloper() {
            return developer;
        }
        public void SetTester(string tester) {
            this.tester = tester;
        }
        public string GetTester() {
            return tester;
        }


    }
}
