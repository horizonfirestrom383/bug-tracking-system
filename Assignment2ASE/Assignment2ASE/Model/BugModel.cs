﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2ASE.Model
{
    class BugModel
    {
        string bugCode,bugstatus, bugtitle,buglines, bugmethods, bugdescription, project, classname, attachment;
        public void SetBugCode(string bugCode)
        {
            this.bugCode = bugCode;

        }
        public string GetBugCode()
        {
            return bugCode;
        }

        public void SetBugStatus(string bugstatus) {
            this.bugstatus = bugstatus;

        }
        public string GetBugStatus() {
            return bugstatus;
        }
        public void SetBugTitle(string bugtitle) {
            this.bugtitle = bugtitle;
        }
        public string GetBugTitle() {
            return bugtitle;
        }

        public void SetBugLines(string lines)
        {
            this.buglines = lines;
        }
        public string GetBugLines()
        {
            return buglines;
        }
        public void SetBugMethod(string method)
        {
            this.bugmethods = method;
        }
        public string GetBugMethod()
        {
            return bugmethods;
        }
        public void SetBugDescription(string bugdescription) {
            this.bugdescription = bugdescription;
        }
        public string GetBugDescription() {
            return bugdescription;
        }
        public void SetProject(string project) {
            this.project = project;
        }
        public string GetProject() {
            return project;
        }
        public void SetBugClass(string classname) {
            this.classname = classname;
        }
        public string GetBugClass() {
            return classname;
        }
        public void SetBugAttachment(string attachment) {
            this.attachment = attachment;
        }
        public string GetBugAttachment() {
            return attachment;
        }
    }
}
