﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2ASE.Model
{
    class ManagerModel
    {
        int manager_id;
        string manager_code, name, department, address, contact_no, email, joined_date, position;
        string searchitem;

        public void setSearchItem(String sitem)
        {
            this.searchitem = sitem;
        }

        public String getSearchItem()
        {
            return searchitem;
        }

        public void setManagerId(int manager_id) {
            this.manager_id = manager_id;
        }
        public int getManagerId() {
            return manager_id;

        }

        public void setManagerCode(string manager_code)
        {
            this.manager_code = manager_code;
        }
        public string getManagerCode() {
            return manager_code;
        }

        public void setManagerName(string manager_name)
        {
            this.name = manager_name;
        }
        public string getManagerName()
        {
            return name;
        }

        public void setDepartment(string department)
        {
            this.department = department;
        }
        public string getDepartment()
        {
            return department;
        }


        public void setAddrress(string address)
        {
            this.address = address;
        }
        public string getAddress()
        {
            return address;
        }

        public void setContactNo(string contact_no)
        {
            this.contact_no = contact_no;
        }
        public string getContactNo()
        {
            return contact_no;
        }


        public void setEmail(string email)
        {
            this.email = email;
        }
        public string getEmail()
        {
            return email;
        }

        public void setJoinedDate(string joineddate)
        {
            this.joined_date = joineddate;
        }
        public string getJoinedDate()
        {
            return joined_date;
        }

        public void setPosition(string position)
        {
            this.position = position;
        }
        public string getPosition()
        {
            return position;
        }

        
    }
}
