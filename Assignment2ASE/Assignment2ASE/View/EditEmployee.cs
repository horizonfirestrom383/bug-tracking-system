﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class EditEmployee : Form
    {
        public EditEmployee()
        {
            InitializeComponent();
        }

        public EditEmployee(string empCode, string empName, string contactNo, string empEmail,string empAddress,
            string empDesignation, string empPosition)
        {
            InitializeComponent();
            // MessageBox.Show("Employee Code is "+ empCode);
            txtEmpCode.Text = empCode;
            txtName.Text = empName;
            txtContactNo.Text = contactNo;
            txtEmail.Text = empEmail;
            txtAddress.Text = empAddress;
            txtDesignation.Text = empDesignation;
            txtPosition.Text = empPosition;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string empCode = txtEmpCode.Text;
            EmployeeModel em = new EmployeeModel();
            em.SetEmployeeCode(empCode);
            ConEmployee ce = new ConEmployee();
            if (ce.Deletedata(em) == true) {
                MessageBox.Show("Employee Deleted Successfully");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtEmpCode.Text ="";
            txtName.Text = "";
            txtContactNo.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtDesignation.Text = "";
            txtPosition.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string empCode =txtEmpCode.Text;
           string name = txtName.Text ;
            string contactno=txtContactNo.Text;
            string email = txtEmail.Text;
            string address = txtAddress.Text;
            string designation = txtDesignation.Text;
            string position = txtPosition.Text;
            EmployeeModel em = new EmployeeModel();
            em.SetEmployeeCode(empCode);
            em.SetName(name);
            em.SetContactno(contactno);
            em.SetEmail(email);
            em.SetAddress(address);
            em.SetDesigntion(designation);
            em.SetPosition(position);

            ConEmployee ce = new ConEmployee();
            if (ce.UpdateEmployee(em) == true) {
                MessageBox.Show("Employee Information Edited Successfully");
            }
        }

        private void Searrch_Click(object sender, EventArgs e)
        {
           string search = txtSearch.Text;
            ConEmployee ce = new ConEmployee();
            EmployeeModel ee = new EmployeeModel();
            ce.Search(search,ee);
            txtEmpCode.Text = ee.GetEmployeeCode();
            txtName.Text = ee.GetName();
            txtContactNo.Text =ee.GetContactno();
            txtEmail.Text =ee.GetEmail();
            txtAddress.Text = ee.GetAddress();
            txtDesignation.Text = ee.GetDesignation();
            txtPosition.Text = ee.GetPosition();

        }
    }
}
