﻿using Assignment2ASE.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class LoginForm : Form
    {
        ConLogin conlogin = new ConLogin();
        public LoginForm()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        

        

        

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void combUserType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

       

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string username = txtUserName.Text;
            string password = txtPassword.Text;
            string usertype = combUserType.Text;

            if (conlogin.Validate(username, password) == true)
            {
                if (usertype == "Admin")
                {
                    AdminForm af = new AdminForm();
                    af.Show();
                    this.Hide();
                }
                else if (usertype == "Manager")
                {
                    ManagerForm mf = new ManagerForm();
                    mf.Show();
                    this.Hide();
                }
                else if (usertype == "Developer")
                {
                    DeveloperForm df = new DeveloperForm();
                    df.Show();
                    this.Hide();
                }
                else if (usertype == "Tester")
                {
                    TesterForm tf = new TesterForm();
                    tf.Show();
                    this.Hide();
                }




            }
            else
            {
                MessageBox.Show("Invalid Username or Password");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
