﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();
            AllEmployee();
            AllManager();
            AllProject();
        }
        public void AllProject()
        {
            ConProject projectController = new ConProject();
            DataTable table = projectController.AllProject();
            dataGridView3.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            //dataGridView1.Columns["joined_date"].Visible = false;
            dataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView3.MultiSelect = false;
        }
        public void AllEmployee()
        {
            ConEmployee employeeController = new ConEmployee();
            DataTable table = employeeController.AllEmployee();
            dataGridView1.DataSource = table;
            dataGridView1.Columns["emp_id"].Visible = false;
            dataGridView1.Columns["emp_image"].Visible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
        }
        public void AllManager()
        {
            ConManager managerController = new ConManager();
            DataTable table = managerController.AllManager();
            dataGridView2.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            //dataGridView1.Columns["joined_date"].Visible = false;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.MultiSelect = false;
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoginForm lf = new LoginForm();
            lf.Show();
            this.Hide();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword cp = new ChangePassword();
            cp.Show();
        }

        private void createManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateManager cm = new CreateManager();
            cm.Show();
        }

        private void editManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditDeleteManager edm = new EditDeleteManager();
            edm.Show();

        }

        private void employeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewAllEmployees allEmployees = new ViewAllEmployees();
            allEmployees.Show();
        }

        private void managerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewAllManagers allManagers = new ViewAllManagers();
            allManagers.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string empCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string empName = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            string contactNo = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            string empEmail = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            string empAddress = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            string empDesignation = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            string empPosition = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            EditEmployee addEmployee = new EditEmployee(empCode, empName, contactNo, empEmail,
                empAddress,empDesignation, empPosition);
            addEmployee.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string empCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            EmployeeModel em = new EmployeeModel();
            em.SetEmployeeCode(empCode);
            ConEmployee ce = new ConEmployee();
            if (ce.Deletedata(em) == true) {
                MessageBox.Show("The Selected Employee Deleted Successfully");
            }
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string mgrCode = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
            string name = dataGridView2.SelectedRows[0].Cells[1].Value.ToString();
            string department = dataGridView2.SelectedRows[0].Cells[2].Value.ToString();
            string address = dataGridView2.SelectedRows[0].Cells[3].Value.ToString();
            string contactNo = dataGridView2.SelectedRows[0].Cells[4].Value.ToString();
            string email = dataGridView2.SelectedRows[0].Cells[5].Value.ToString();
            string position = dataGridView2.SelectedRows[0].Cells[6].Value.ToString();
            EditDeleteManager edm = new EditDeleteManager(mgrCode, name, department, address, contactNo, email);
            edm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string mgrCode = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
            MessageBox.Show(mgrCode);
            ManagerModel m = new ManagerModel();
            m.setManagerCode(mgrCode);
            ConManager cm = new ConManager();
            cm.Delete(m);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string projectCode = dataGridView3.SelectedRows[0].Cells[0].Value.ToString();
            string projectTitle = dataGridView3.SelectedRows[0].Cells[1].Value.ToString();
            string projectManager = dataGridView3.SelectedRows[0].Cells[2].Value.ToString();
            string projectDeveloper = dataGridView3.SelectedRows[0].Cells[3].Value.ToString();
            string projectTester = dataGridView3.SelectedRows[0].Cells[4].Value.ToString();
           
            EditProject ep = new EditProject(projectCode, projectTitle, projectManager, projectDeveloper, projectTester);
            
           ep.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string empCode = dataGridView3.SelectedRows[0].Cells[0].Value.ToString();
            //ProjectModel pm = new ProjectModel();
            //pm.SetProjectCode(empCode);
            ConProject cp = new ConProject();
            cp.Delete(empCode);

           
        }

        private void unfixedBugToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
