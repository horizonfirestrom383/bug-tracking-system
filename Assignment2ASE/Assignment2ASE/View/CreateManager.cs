﻿using Assignment2ASE.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class CreateManager : Form
    {
        public CreateManager()
        {
            InitializeComponent();
        }

        public CreateManager(string mgrCode, string name, string department, string address,
                string contactno, string email, string position)
        {
            InitializeComponent();
            txtmanagercode.Text = mgrCode;
            txtName.Text = name;
            txtDepartment.Text = department;
            txtAddress.Text = address;
            txtContactno.Text = contactno;
            txtEmail.Text = email;
            txtPosition.Text = position;
        }



        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string code = txtmanagercode.Text;
            string name = txtName.Text;
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            string usertype = txtUsertype.Text;
            string department = txtDepartment.Text;
            string address = txtAddress.Text;
            string contactno = txtContactno.Text;
            string email = txtEmail.Text;
            
            string position = txtPosition.Text;

            ConManager conmgr = new ConManager();
            if (conmgr.Insertdata(code,name, department, address, contactno, email, position) == true && conmgr.InsertintoUser(username, password, usertype) == true) {
                MessageBox.Show("Successfully Manager Created");
            }

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtName.Text = "";
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtDepartment.Text = "";
            txtUsertype.Text = "";
            txtContactno.Text = "";
            txtAddress.Text = "";
            txtEmail.Text = "";
            //txtJoinedDate.Text = "";
            txtPosition.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
