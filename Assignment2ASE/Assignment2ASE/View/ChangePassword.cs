﻿using Assignment2ASE.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class ChangePassword : Form
    {
        ConChangePassword ccp = new ConChangePassword();
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string oldpassword = txtOldPassword.Text;
            string newpassword = txtNewPassword.Text;
            string configpassword = txtConfigPassword.Text;
            
                if (newpassword == configpassword)
                {
                if (ccp.ChangePassword(username, oldpassword, newpassword) == true) {
                    MessageBox.Show("Successfully Password is Changed");

                }
                }
                else {
                    MessageBox.Show("New Password and Confirm Password are not same");
                }
            
        }
    }
}
