﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class EditProject : Form
    {
        public EditProject()
        {
            InitializeComponent();
        }

        public EditProject(string projectCode, string projectTitle,string projectManager,string projectDeveloper,string projectTester)
        {
            InitializeComponent();
            txtProjectCode.Text = projectCode;
            txtProjectTitle.Text = projectTitle;
            txtManagerName.Text = projectManager;
            txtDeveloper.Text = projectDeveloper;
            txtTester.Text = projectTester;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
          string  search = txtSearch.Text;
            ConProject pe = new ConProject();
            ProjectModel pm = new ProjectModel();

            pe.Search(search, pm);
            txtProjectCode.Text = pm.GetProjectCode();
            txtProjectTitle.Text = pm.GetProjectTitle();
            txtManagerName.Text = pm.GetManager();
            txtDeveloper.Text = pm.GetDeveloper();
            txtTester.Text = pm.GetTester();
            

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            txtProjectCode.Text = "";
            txtProjectTitle.Text = "";
            txtManagerName.Text = "";
            txtDeveloper.Text = "";
            txtTester.Text = "";

               
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string projectCode = txtProjectCode.Text;
            ConProject cp = new ConProject();
            cp.Delete(projectCode);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string projectCode = txtProjectCode.Text;
            string projectTitle = txtProjectTitle.Text;
            string projectManager = txtManagerName.Text;
            string projectDeveloper = txtDeveloper.Text;
            string projectTester = txtTester.Text;
            ProjectModel pm = new ProjectModel();
            pm.SetProjectCode(projectCode);
            pm.SetProjectTitle(projectTitle);
            pm.SetManager(projectManager);
            pm.SetDeveloper(projectDeveloper);
            pm.SetTester(projectTester);

            ConProject cp = new ConProject();
            if (cp.UpdateProject(pm) == true)
            {
                MessageBox.Show("Project Details Updated Successfully");
            }
            else {
                MessageBox.Show("There is something wrong");

            }
        }
    }
}
