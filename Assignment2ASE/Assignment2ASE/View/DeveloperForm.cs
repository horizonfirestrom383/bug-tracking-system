﻿using Assignment2ASE.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class DeveloperForm : Form
    {
        public DeveloperForm()
        {
            InitializeComponent();
            AllBugs();
            FixedBugs();
            UnfixedBugs();

        }
        public void AllBugs()
        {
            ConBug bugController = new ConBug();
            DataTable table = bugController.AllBugs();
            dataGridView1.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            dataGridView1.Columns["bug_id"].Visible = false;
            dataGridView1.Columns["Attachment"].Visible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
        }
        public void FixedBugs()
        {
            ConBug bugController = new ConBug();
            DataTable table = bugController.FixedBug();
            dataGridView2.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            dataGridView2.Columns["bug_id"].Visible = false;
            dataGridView2.Columns["Attachment"].Visible = false;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.MultiSelect = false;
        }
        public void UnfixedBugs()
        {
            ConBug bugController = new ConBug();
            DataTable table = bugController.UnFixedBugs();
            dataGridView3.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            dataGridView3.Columns["bug_id"].Visible = false;
            dataGridView3.Columns["Attachment"].Visible = false;
            dataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView3.MultiSelect = false;
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoginForm lf = new LoginForm();
            lf.Show();
            this.Hide();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void createBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateBug cb = new CreateBug();
            cb.Show();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword cp = new ChangePassword();
            cp.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string bugStatus = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            string bugTitle = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            string bugLines = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            string bugMethod = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            string bugDescription = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            string project = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            string bugClass = dataGridView1.SelectedRows[0].Cells[8].Value.ToString();
            // string empPosition = dataGridView3.SelectedRows[0].Cells[7].Value.ToString();
            EditBug eb = new EditBug(bugCode, bugStatus, bugTitle, bugLines, bugMethod, bugDescription, project, bugClass);


            eb.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView2.SelectedRows[0].Cells[1].Value.ToString();
            string bugStatus = dataGridView2.SelectedRows[0].Cells[2].Value.ToString();
            string bugTitle = dataGridView2.SelectedRows[0].Cells[3].Value.ToString();
            string bugLines = dataGridView2.SelectedRows[0].Cells[4].Value.ToString();
            string bugMethod = dataGridView2.SelectedRows[0].Cells[5].Value.ToString();
            string bugDescription = dataGridView2.SelectedRows[0].Cells[6].Value.ToString();
            string project = dataGridView2.SelectedRows[0].Cells[7].Value.ToString();
            string bugClass = dataGridView2.SelectedRows[0].Cells[8].Value.ToString();
            // string empPosition = dataGridView3.SelectedRows[0].Cells[7].Value.ToString();
            EditBug eb = new EditBug(bugCode, bugStatus, bugTitle, bugLines, bugMethod, bugDescription, project, bugClass);


            eb.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView3.SelectedRows[0].Cells[1].Value.ToString();
            string bugStatus = dataGridView3.SelectedRows[0].Cells[2].Value.ToString();
            string bugTitle = dataGridView3.SelectedRows[0].Cells[3].Value.ToString();
            string bugLines = dataGridView3.SelectedRows[0].Cells[4].Value.ToString();
            string bugMethod = dataGridView3.SelectedRows[0].Cells[5].Value.ToString();
            string bugDescription = dataGridView3.SelectedRows[0].Cells[6].Value.ToString();
            string project = dataGridView3.SelectedRows[0].Cells[7].Value.ToString();
            string bugClass = dataGridView3.SelectedRows[0].Cells[8].Value.ToString();
            // string empPosition = dataGridView3.SelectedRows[0].Cells[7].Value.ToString();
            EditBug eb = new EditBug(bugCode, bugStatus, bugTitle, bugLines, bugMethod, bugDescription, project, bugClass);


            eb.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            // em = new EmployeeModel();
            //em.SetEmployeeCode(empCode);
            ConBug cb = new ConBug();
            if (cb.Deletedata(bugCode) == true)
            {
                MessageBox.Show("The Selected Bug Deleted Successfully");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView2.SelectedRows[0].Cells[1].Value.ToString();
            // em = new EmployeeModel();
            //em.SetEmployeeCode(empCode);
            ConBug cb = new ConBug();
            if (cb.Deletedata(bugCode) == true)
            {
                MessageBox.Show("The Selected Bug Deleted Successfully");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView3.SelectedRows[0].Cells[1].Value.ToString();
            // em = new EmployeeModel();
            //em.SetEmployeeCode(empCode);
            ConBug cb = new ConBug();
            if (cb.Deletedata(bugCode) == true)
            {
                MessageBox.Show("The Selected Bug Deleted Successfully");
            }
        }

        private void editBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditBug eb = new EditBug();
            eb.Show();
        }

        private void viewBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewAllBugs vb = new ViewAllBugs();
            vb.Show();
        }
    }
}
