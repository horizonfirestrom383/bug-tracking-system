﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Assignment2ASE.View
{
    public partial class CreateBug : Form
    {
        public Regex keyWords = new Regex("abstract|as|base|bool|break|byte|case|catch|char|checked|class|const|continue|decimal|default|delegate|do|double|else|enum|event|explicit|extern|false|finally|fixed|float|for|"
                               + "foreach|goto|if|implicit|in|int|interface|internal|is|lock|long|namespace|new|null|object|operator|out|override|params|private|protected|public|readonly|ref|return|sbyte|sealed|short|sizeof|stackalloc|static|"
                               + "string|struct|switch|this|throw|true|try|typeof|uint|ulong|unchecked|unsafe|ushort|using|virtual|volatile|void|while|");


        public CreateBug()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CreateBug_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BugModel bm = new BugModel();
            bm.SetBugCode(txtBugCode.Text);
            bm.SetBugStatus(txtbugStatus.Text);
            bm.SetBugTitle(txtbugTitle.Text);
            bm.SetBugLines(txtLinesOfCode.Text);
            bm.SetBugMethod(txtmethod.Text);
            bm.SetBugDescription(txtbugDescription.Text);
            bm.SetProject(txtProject.Text);
            bm.SetBugClass(txtClass.Text);
            bm.SetBugAttachment(txtAttachment.Text);

            Bitmap image = new Bitmap(pictureBox1.Image);
            byte[] arr;
            ImageConverter converter = new ImageConverter();
            arr = (byte[])converter.ConvertTo(image, typeof(byte[]));
            ConBug cb = new ConBug();
            if (cb.InsertBug(bm, arr) == true)
            {
                MessageBox.Show("Bug Added Successfully");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // open file dialog   
            OpenFileDialog open = new OpenFileDialog()
            {
                // image filters  
                Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp"
            };
            if (open.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box  
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox1.Image = new Bitmap(open.FileName);
                // image file path  
                txtAttachment.Text = open.FileName;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //Highlight every found word from keyWords.




            //Get the last cursor position in the richTextBox1.




            int selPos = txtbugDescription.SelectionStart;




            //For each match from the regex, highlight the word.

            foreach (Match keyWordMatch in keyWords.Matches(txtbugDescription.Text))

            {




                txtbugDescription.Select(keyWordMatch.Index, keyWordMatch.Length);

                txtbugDescription.SelectionColor = Color.Blue;

                txtbugDescription.SelectionStart = selPos;

                txtbugDescription.SelectionColor = Color.Black;
            }
        }

        private void txtbugDescription_TextChanged(object sender, EventArgs e)
        {
            //Highlight every found word from keyWords.

            //Get the last cursor position in the richTextBox1.

            int selPos = txtbugDescription.SelectionStart;



            //For each match from the regex, highlight the word.

            foreach (Match keyWordMatch in keyWords.Matches(txtbugDescription.Text))

            {




                txtbugDescription.Select(keyWordMatch.Index, keyWordMatch.Length);

                txtbugDescription.SelectionColor = Color.Blue;

                txtbugDescription.SelectionStart = selPos;

                txtbugDescription.SelectionColor = Color.Black;

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            txtBugCode.Text = "";
            txtbugStatus.Text = "";
            txtbugTitle.Text = "";
            txtLinesOfCode.Text = "";
            txtmethod.Text = "";
            txtbugDescription.Text = "";
            txtClass.Text = "";
            txtProject.Text = "";
            txtAttachment.Text = "";


        }
    }
}