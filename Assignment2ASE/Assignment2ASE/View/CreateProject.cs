﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class CreateProject : Form
    {
        public CreateProject()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            ProjectModel pm = new ProjectModel();
            pm.SetProjectTitle(txtttitle.Text);
            pm.SetProjectCode(txtCode.Text);
            //pm.SetProjectStartDate(txtStartDate.Value.ToString("yyyy - MM - dd"));
            pm.SetManager(txtManager.Text);
            pm.SetDeveloper(txtDeveloper.Text);
            pm.SetTester(txtTester.Text);
            ConProject cp = new ConProject();
            if (cp.InsertProject(pm) ==true) {
                MessageBox.Show("Project Created Successfully");
                
            }
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtCode.Text = "";
            txtttitle.Text = "";
            txtManager.Text = "";
            txtDeveloper.Text = "";
            txtTester.Text = "";
        }
    }
}
