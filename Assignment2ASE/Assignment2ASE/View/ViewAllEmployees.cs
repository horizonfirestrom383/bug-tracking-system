﻿using Assignment2ASE.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class ViewAllEmployees : Form
    {
        public ViewAllEmployees()
        {
            InitializeComponent();
            AllEmployee();
        }

        public void AllEmployee()
        {
            ConEmployee employeeController = new ConEmployee();
            DataTable table=employeeController.AllEmployee();
            dataGridView1.DataSource = table;
            dataGridView1.Columns["emp_id"].Visible = false;
            dataGridView1.Columns["emp_image"].Visible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string empCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string empName = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            string contactNo = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            string empEmail = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            string empAddress = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            string empDesignation = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            string empPosition = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            EditEmployee editEmployee = new EditEmployee(empCode, empName, contactNo, empEmail,empAddress,
                empDesignation, empPosition);
            editEmployee.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            // em = new EmployeeModel();
            //em.SetEmployeeCode(empCode);
            ConBug cb = new ConBug();
            if (cb.Deletedata(bugCode) == true)
            {
                MessageBox.Show("The Selected Bug Deleted Successfully");
            }
        }
    }
}
