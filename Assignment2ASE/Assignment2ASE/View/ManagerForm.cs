﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class ManagerForm : Form
    {
        
        public ManagerForm()
        {
            InitializeComponent();
            AllProject();
            AllEmployee();
            AllBugs();
        }
        public void AllProject()
        {
            ConProject projectController = new ConProject();
            DataTable table = projectController.AllProject();
            dataGridView2.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            //dataGridView1.Columns["joined_date"].Visible = false;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.MultiSelect = false;
        }
        public void AllEmployee()
        {
            ConEmployee employeeController = new ConEmployee();
            DataTable table = employeeController.AllEmployee();
            dataGridView1.DataSource = table;
            dataGridView1.Columns["emp_id"].Visible = false;
            dataGridView1.Columns["emp_image"].Visible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
        }
        public void AllBugs()
        {
            ConBug bugController = new ConBug();
            DataTable table = bugController.AllBugs();
            dataGridView3.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            dataGridView3.Columns["bug_id"].Visible = false;
            dataGridView3.Columns["Attachment"].Visible = false;
            dataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView3.MultiSelect = false;
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoginForm lf = new LoginForm();
            lf.Show();
            this.Hide();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void createEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateProject cp = new CreateProject();
            cp.Show();
                
        }

        private void createBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateBug cb = new CreateBug();
            cb.Show();
        }

        private void createEmployeeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddEmployee ae = new AddEmployee();
            ae.Show();
        }

        private void editEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditEmployee ee = new EditEmployee();
            ee.Show();
        }

        private void editBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditBug eb = new EditBug();
            eb.Show();
        }

        private void editProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditProject ep = new EditProject();
            ep.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string empCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            EmployeeModel em = new EmployeeModel();
            em.SetEmployeeCode(empCode);
            ConEmployee ce = new ConEmployee();
            if (ce.Deletedata(em) == true)
            {
                MessageBox.Show("The Selected Employee Deleted Successfully");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string empCode = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
            //ProjectModel pm = new ProjectModel();
            //pm.SetProjectCode(empCode);
            ConProject cp = new ConProject();
            cp.Delete(empCode);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string empCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string empName = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            string contactNo = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            string empEmail = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            string empAddress = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            string empDesignation = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            string empPosition = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            EditEmployee addEmployee = new EditEmployee(empCode, empName, contactNo, empEmail,
                empAddress, empDesignation, empPosition);
            addEmployee.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string projectCode = dataGridView3.SelectedRows[0].Cells[0].Value.ToString();
            string projectTitle = dataGridView3.SelectedRows[0].Cells[1].Value.ToString();
            string projectManager = dataGridView3.SelectedRows[0].Cells[2].Value.ToString();
            string projectDeveloper = dataGridView3.SelectedRows[0].Cells[3].Value.ToString();
            string projectTester = dataGridView3.SelectedRows[0].Cells[4].Value.ToString();

            EditProject ep = new EditProject(projectCode, projectTitle, projectManager, projectDeveloper, projectTester);

            ep.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView3.SelectedRows[0].Cells[1].Value.ToString();
            string bugStatus = dataGridView3.SelectedRows[0].Cells[2].Value.ToString();
            string bugTitle = dataGridView3.SelectedRows[0].Cells[3].Value.ToString();
            string buglines = dataGridView3.SelectedRows[0].Cells[4].Value.ToString();
            string bugmethod = dataGridView3.SelectedRows[0].Cells[5].Value.ToString();
            string bugDescription = dataGridView3.SelectedRows[0].Cells[6].Value.ToString();
            string project = dataGridView3.SelectedRows[0].Cells[7].Value.ToString();
            string bugclass = dataGridView3.SelectedRows[0].Cells[8].Value.ToString();
            // string empPosition = dataGridView3.SelectedRows[0].Cells[7].Value.ToString();
            EditBug eb = new EditBug(bugCode, bugStatus, bugTitle, buglines, bugmethod, bugDescription, project,bugclass);
            

            eb.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string bugCode = dataGridView3.SelectedRows[0].Cells[1].Value.ToString();
            // em = new EmployeeModel();
            //em.SetEmployeeCode(empCode);
            ConBug cb = new ConBug();
            if (cb.Deletedata(bugCode) == true)
            {
                MessageBox.Show("The Selected Bug Deleted Successfully");
            }
        }

        private void viewBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewAllBugs vb = new ViewAllBugs();
            vb.Show();
        }

        private void bugToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ViewAllBugs vb = new ViewAllBugs();
            vb.Show();

        }

        private void employeeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ViewAllEmployees ve = new ViewAllEmployees();
            ve.Show();
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void changePassworddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword cp = new ChangePassword();
            cp.Show();
        }
    }
}
