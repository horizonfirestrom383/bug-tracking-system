﻿using Assignment2ASE.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class ViewAllBugs : Form
    {
        public ViewAllBugs()
        {
            InitializeComponent();
            AllBugs();
        }
        public void AllBugs()
        {
            ConBug bugController = new ConBug();
            DataTable table = bugController.AllBugs();
            dataGridView1.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            dataGridView1.Columns["bug_id"].Visible = false;
            dataGridView1.Columns["Attachment"].Visible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string bugCode = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string bugStatus = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            string bugTitle = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            string bugLines = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            string bugMethod = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            string bugDescription = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            string project = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            string bugClass = dataGridView1.SelectedRows[0].Cells[8].Value.ToString();
            // string empPosition = dataGridView3.SelectedRows[0].Cells[7].Value.ToString();
            EditBug eb = new EditBug(bugCode, bugStatus, bugTitle, bugLines, bugMethod, bugDescription, project, bugClass);


            eb.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
