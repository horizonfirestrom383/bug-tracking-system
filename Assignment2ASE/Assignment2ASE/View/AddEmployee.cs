﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class AddEmployee : Form
    {
        public AddEmployee()
        {
            InitializeComponent();
        }

        

        private void btnbrowse_Click(object sender, EventArgs e)
        {
            // open file dialog   
            OpenFileDialog open = new OpenFileDialog();
            // image filters  
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box  
                pictureboximage.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureboximage.Image = new Bitmap(open.FileName);
                // image file path  
                //textBox1.Text = open.FileName;
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            
             EmployeeModel em = new EmployeeModel();
            em.SetEmployeeCode(txtemployeecode.Text);
            em.SetName(txtname.Text);
            em.SetUsername(txtUsername.Text);
            em.SetPassword(txtpassword.Text);
            em.SetUsertype(txtUserType.Text);
            em.SetContactno(txtContactNo.Text);
            em.SetEmail(txtEmail.Text);
            em.SetAddress(txtAddress.Text);
            em.SetDesigntion(txtDesignation.Text);
            em.SetPosition(txtposition.Text);

            Bitmap image = new Bitmap(pictureboximage.Image);
            byte[] arr;
            ImageConverter converter = new ImageConverter();
            arr = (byte[])converter.ConvertTo(image, typeof(byte[]));

            if (txtpassword.Text == txtconfirmpassword.Text)
            {
                ConEmployee ce = new ConEmployee();
                if (ce.Insertdata(em, arr) == true) {
                    MessageBox.Show("Employee Created successfully");
                }
            }
            else {
                MessageBox.Show("Password and Confirm Password are not same");
            }




        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            txtemployeecode.Text = "";
            txtname.Text = "";
            txtUsername.Text = "";
            txtpassword.Text = "";
            txtUserType.Text = "";
            txtContactNo.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtDesignation.Text = "";
            txtposition.Text = "";
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
