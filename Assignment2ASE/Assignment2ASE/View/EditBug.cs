﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class EditBug : Form
    {
        public EditBug(string bugCode, string bugStatus,string  bugTitle, string buglines, string bugmethod,string  bugDescription,string  project,string bugclass)
        {
            InitializeComponent();
            txtBugCode.Text = bugCode;
            txtBugStatus.Text = bugStatus;
            txtBugTitle.Text = bugTitle;
            txtLinesOfCode.Text = buglines;
            txtNameOfMethods.Text = bugmethod;
            txtBugDescription.Text = bugDescription;
            txtProject.Text = project;
            txtClass.Text = bugclass;
        }
        public EditBug()
        {
            InitializeComponent();
        }

        private void Search_Click(object sender, EventArgs e)
        {
           string searchItem = txtSearch.Text;
            BugModel bm = new BugModel();
            ConBug cb = new ConBug();
            cb.Search(searchItem, bm);
            txtBugCode.Text = bm.GetBugCode();
            txtBugStatus.Text = bm.GetBugStatus();
            txtBugTitle.Text = bm.GetBugTitle();
            txtLinesOfCode.Text = bm.GetBugLines();
            txtNameOfMethods.Text = bm.GetBugMethod();
            txtBugDescription.Text = bm.GetBugDescription();
            txtProject.Text = bm.GetProject();
            txtClass.Text = bm.GetBugClass();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            txtBugCode.Text = "";
            txtBugStatus.Text = "";
            txtBugTitle.Text = "";
            txtBugDescription.Text = "";
            txtProject.Text = "";
            txtClass.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string bugCode = txtBugCode.Text;
            ConBug cb = new ConBug();
            if (cb.Deletedata(bugCode) == true) {
                MessageBox.Show("Bug Deleted Successfully");
            }
        }
    }
}
