﻿using Assignment2ASE.Controller;
using Assignment2ASE.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class EditDeleteManager : Form
    {



        public EditDeleteManager(string mgrCode, string name, string department, string address, string contactNo, string email)
        {
            InitializeComponent();
            txtmgrcode.Text = mgrCode;
            txtname.Text = name;
            txtdepartment.Text = department;
            txtaddress.Text = address;
            txtcontactno.Text = contactNo;
            txtemail.Text = email;
           // txtPosition.Text = position;
        }

        public EditDeleteManager()
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            ManagerModel m = new ManagerModel();
            m.setSearchItem(this.txtSearch.Text);
            ConManager cm = new ConManager();
            cm.Search(m);

            txtmgrcode.Text = m.getManagerCode();
            txtname.Text = m.getManagerName();
            txtdepartment.Text =m.getDepartment();
            txtcontactno.Text = m.getContactNo();
            txtaddress.Text = m.getAddress();
            txtemail.Text = m.getEmail();
           // txtJoinedDate.Text =m.getJoinedDate();
            txtPosition.Text = m.getPosition();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            txtmgrcode.Text = "";
            txtname.Text = "";
            txtdepartment.Text = "";
            txtcontactno.Text = "";
            txtaddress.Text = "";
            txtemail.Text = "";
            //txtJoinedDate.Text = "";
            txtPosition.Text = "";
            
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ManagerModel m = new ManagerModel();
            m.setManagerCode(this.txtmgrcode.Text);
            ConManager cm = new ConManager();
            cm.Delete(m);
            txtmgrcode.Text = "";
            txtname.Text = "";
            txtdepartment.Text = "";
            txtcontactno.Text = "";
            txtaddress.Text = "";
            txtemail.Text = "";
            //txtJoinedDate.Text = "";
            txtPosition.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ManagerModel m = new ManagerModel();
            m.setManagerCode(this.txtmgrcode.Text);
            m.setManagerName(this.txtname.Text);
            m.setDepartment(this.txtdepartment.Text);
            m.setContactNo(this.txtcontactno.Text);
            m.setAddrress(this.txtaddress.Text);
            m.setEmail(this.txtemail.Text);
            //m.setJoinedDate(this.txtJoinedDate.Text);
            m.setPosition(this.txtPosition.Text);

            ConManager cm = new ConManager();
            cm.Update(m);
    
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
