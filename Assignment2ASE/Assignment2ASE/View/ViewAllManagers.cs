﻿using Assignment2ASE.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2ASE.View
{
    public partial class ViewAllManagers : Form
    {
        public ViewAllManagers()
        {
            InitializeComponent();
            AllManager();
        }
        public void AllManager()
        {
            ConManager managerController = new ConManager();
            DataTable table = managerController.AllManager();
            dataGridView1.DataSource = table;
            //dataGridView1.Columns["emp_id"].Visible = false;
            //dataGridView1.Columns["emp_image"].Visible = false;
            //dataGridView1.Columns["joined_date"].Visible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }
    }
}
