﻿namespace Assignment2ASE.View
{
    partial class CreateBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbugTitle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbugDescription = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAttachment = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.txtbugStatus = new System.Windows.Forms.ComboBox();
            this.txtProject = new System.Windows.Forms.TextBox();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBugCode = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtLinesOfCode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtmethod = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(323, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Create Bug";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enter Bug Details";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Bug Status";
            // 
            // txtbugTitle
            // 
            this.txtbugTitle.Location = new System.Drawing.Point(216, 144);
            this.txtbugTitle.Name = "txtbugTitle";
            this.txtbugTitle.Size = new System.Drawing.Size(279, 20);
            this.txtbugTitle.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Bug Title";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Bug Description";
            // 
            // txtbugDescription
            // 
            this.txtbugDescription.Location = new System.Drawing.Point(216, 224);
            this.txtbugDescription.Name = "txtbugDescription";
            this.txtbugDescription.Size = new System.Drawing.Size(620, 145);
            this.txtbugDescription.TabIndex = 7;
            this.txtbugDescription.Text = "";
            this.txtbugDescription.TextChanged += new System.EventHandler(this.txtbugDescription_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 447);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Project";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(46, 408);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Class";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(47, 484);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Attachment";
            // 
            // txtAttachment
            // 
            this.txtAttachment.Location = new System.Drawing.Point(216, 481);
            this.txtAttachment.Name = "txtAttachment";
            this.txtAttachment.Size = new System.Drawing.Size(279, 20);
            this.txtAttachment.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(509, 481);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(172, 19);
            this.button1.TabIndex = 14;
            this.button1.Text = "Browse...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(637, 530);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(172, 30);
            this.button2.TabIndex = 15;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(216, 530);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(172, 30);
            this.button3.TabIndex = 16;
            this.button3.Text = "Create Bug";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtbugStatus
            // 
            this.txtbugStatus.FormattingEnabled = true;
            this.txtbugStatus.Items.AddRange(new object[] {
            "Fixed",
            "Unfixed"});
            this.txtbugStatus.Location = new System.Drawing.Point(214, 117);
            this.txtbugStatus.Name = "txtbugStatus";
            this.txtbugStatus.Size = new System.Drawing.Size(281, 21);
            this.txtbugStatus.TabIndex = 17;
            // 
            // txtProject
            // 
            this.txtProject.Location = new System.Drawing.Point(216, 437);
            this.txtProject.Name = "txtProject";
            this.txtProject.Size = new System.Drawing.Size(277, 20);
            this.txtProject.TabIndex = 18;
            // 
            // txtClass
            // 
            this.txtClass.Location = new System.Drawing.Point(214, 402);
            this.txtClass.Name = "txtClass";
            this.txtClass.Size = new System.Drawing.Size(278, 20);
            this.txtClass.TabIndex = 19;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(510, 375);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(325, 96);
            this.panel1.TabIndex = 20;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(6, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(316, 76);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(45, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Bug Code";
            // 
            // txtBugCode
            // 
            this.txtBugCode.Location = new System.Drawing.Point(214, 90);
            this.txtBugCode.Name = "txtBugCode";
            this.txtBugCode.Size = new System.Drawing.Size(279, 20);
            this.txtBugCode.TabIndex = 22;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtLinesOfCode
            // 
            this.txtLinesOfCode.Location = new System.Drawing.Point(217, 170);
            this.txtLinesOfCode.Name = "txtLinesOfCode";
            this.txtLinesOfCode.Size = new System.Drawing.Size(279, 20);
            this.txtLinesOfCode.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(45, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Lines Of Code";
            // 
            // txtmethod
            // 
            this.txtmethod.Location = new System.Drawing.Point(217, 198);
            this.txtmethod.Name = "txtmethod";
            this.txtmethod.Size = new System.Drawing.Size(279, 20);
            this.txtmethod.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(45, 205);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Name Of Method";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(429, 530);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(172, 30);
            this.button4.TabIndex = 27;
            this.button4.Text = "Clear";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // CreateBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 572);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtmethod);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtLinesOfCode);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtBugCode);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtClass);
            this.Controls.Add(this.txtProject);
            this.Controls.Add(this.txtbugStatus);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtAttachment);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtbugDescription);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtbugTitle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CreateBug";
            this.Text = "CreateBug";
            this.Load += new System.EventHandler(this.CreateBug_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbugTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox txtbugDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAttachment;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox txtbugStatus;
        private System.Windows.Forms.TextBox txtProject;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBugCode;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox txtLinesOfCode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtmethod;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button4;
    }
}